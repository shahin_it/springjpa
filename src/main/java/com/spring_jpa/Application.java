package com.spring_jpa;

import com.spring_jpa.config.EntityBase;
import com.spring_jpa.model.ModelBase;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;

@SpringBootApplication
public class Application {
    @Autowired
    ApplicationContext appCtx;
    @Autowired
    Environment env;
    @Autowired
    private EntityManagerFactory entityManagerFactory;
    @Autowired
    EntityBase entityBase;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    private void init() {
        SessionFactory sf = entityManagerFactory.unwrap(SessionFactory.class);
        entityBase.sessionFactory = sf;
        ModelBase.entityBase = entityBase;
        entityManagerFactory.getMetamodel().getEntities().forEach(entity -> {
            Class clazz = entity.getJavaType();
            try {
                clazz.getMethod("_init", null).invoke(null);
            } catch (NoSuchMethodException e) {
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}