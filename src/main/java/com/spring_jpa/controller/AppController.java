package com.spring_jpa.controller;

import com.spring_jpa.config.CriteriaManager;
import com.spring_jpa.config.EntityBase;
import com.spring_jpa.form.ProductForm;
import com.spring_jpa.model.Product;
import com.spring_jpa.util.HMACClient;
import com.spring_jpa.util.HmacUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class AppController {

    final EntityBase entityBase;

    public AppController(EntityBase entityBase) {
        this.entityBase = entityBase;
    }

    @ResponseBody
    @RequestMapping("")
    public Map index(@RequestParam Map params) {
        Map model = new LinkedHashMap();
        CriteriaManager<Product> cm = entityBase.createCriteria(Product.class);
        cm.with((cb, root) -> cm.where(
                cb.gt(root.get("id"), 10),
                cb.lt(root.get("id"), 20),
                cb.like(root.get("name"), "Pro%")
        ));

        model.put("items", cm.list(params));
        model.put("count", cm.count(params));
        return model;
    }

    @RequestMapping("/list/{url}/*")
    public Map list(@RequestParam Map params, @PathVariable String url) {
        Map model = new LinkedHashMap();
        CriteriaManager<Product> cm = entityBase.createCriteria(Product.class);
        cm.where(cm.criteriaBuilder.gt(cm.root.get("id"), 0), cm.criteriaBuilder.lt(cm.root.get("id"), 10))
                .where(cm.criteriaBuilder.like(cm.root.get("name"), "Pro%"));
        model.put("items", cm.list(params));
        model.put("count", cm.count(params));
        return model;
    }

    @ResponseBody
    @RequestMapping("/pList")
    public Map productList() {
        Map model = new LinkedHashMap();
        SessionFactory sessionFactory = EntityBase.sessionFactory;
        Session session = sessionFactory.openSession();
        DetachedCriteria criteria = DetachedCriteria.forClass(Product.class).add(Restrictions.like("name", "%ProductForm%"));
        model.put("items", criteria.getExecutableCriteria(session).list());
        return model;
    }

    @RequestMapping("/addProduct")
    public String addProduct(ProductForm productForm) {
        return "addProduct";
    }

    @RequestMapping("/saveProduct")
    public Object saveProduct(@Valid ProductForm productForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "addProduct";
        }
        return "redirect:/pList";
    }

    @ResponseBody
    @RequestMapping("/postHmac")
    public Map postHmac(HttpServletRequest request) {
        Logger LOG = LoggerFactory.getLogger(HMACClient.class);
        String API_KEY = "sdfbjshbf87e564378jkdfghu854y7t54ghkdfjg";
        String APPID = "rds-remote-recovery";
        String HMAC_HEADER = "Hmac";
        String CONTENT_TYPE_HEADER = "content-type";
        String DATE_HEADER = "Date";
        String SECRET_API_KEY = "secretsecret";

        String MD5 = "MD5";
        String HMACSHA1 = "HmacSHA1";

        String receivedHmac = request.getHeader(HMAC_HEADER);
        if (receivedHmac.contains(":") && receivedHmac.split(":").length == 3) {
            String[] headerParts = receivedHmac.split(":");
            String user = headerParts[0];
            String hmac = headerParts[1];
            String nonce = headerParts[2];
            try {
                String body = request.getReader().lines().collect(Collectors.joining());
                String data = HmacUtil.createHmacHeader(APPID, request.getContextPath(), body,
                        request.getMethod(), request.getHeader(DATE_HEADER), nonce);
                String calculatedHMAC = HmacUtil.calculateHMAC(data, SECRET_API_KEY);
                if (hmac.equals(calculatedHMAC)) {
                    return new HashMap() {{
                        put("success", true);
                    }};
                }
            } catch (IOException | NoSuchAlgorithmException | InvalidKeyException e) {
                e.printStackTrace();
            }
        }

        return new HashMap() {{
            put("success", false);
        }};
    }

}
