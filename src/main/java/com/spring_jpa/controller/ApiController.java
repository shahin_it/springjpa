package com.spring_jpa.controller;

import com.spring_jpa.model.Tutorial;
import com.spring_jpa.repo.TutorialRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ApiController {
    @Autowired
    TutorialRepo tutorialRepo;

    @GetMapping("/tutorials")
    public ResponseEntity<List<Tutorial>> getAllTutorials(@RequestParam(required = false) String title) {
        return ResponseEntity.ok(tutorialRepo.findAll());
    }

    @GetMapping("/tutorials/{id}")
    public ResponseEntity<Tutorial> getTutorialById(@PathVariable("id") long id) {
        return ResponseEntity.ok(tutorialRepo.getById(id));
    }

    @PostMapping("/tutorials")
    public ResponseEntity<Tutorial> createTutorial(@RequestBody Tutorial tutorial) {
        return ResponseEntity.ok(tutorialRepo.save(tutorial));
    }

    @PutMapping("/tutorials/{id}")
    public ResponseEntity<Tutorial> updateTutorial(@PathVariable("id") long id, @RequestBody Tutorial tutorial) {
        Tutorial dbTutorial = tutorialRepo.getById(id);
        dbTutorial.setTitle(tutorial.getTitle());
        dbTutorial.setDescription(tutorial.getDescription());
        return ResponseEntity.ok(dbTutorial);
    }

    @DeleteMapping("/tutorials/{id}")
    public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
        tutorialRepo.delete(tutorialRepo.getById(id));
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/tutorials")
    public ResponseEntity<HttpStatus> deleteAllTutorials() {
        tutorialRepo.deleteAll();
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

    @GetMapping("/tutorials/published")
    public ResponseEntity<List<Tutorial>> findByPublished() {
        List<Tutorial> tutorials = tutorialRepo.findByPublished(true);
        return ResponseEntity.ok(tutorials);
    }
}
