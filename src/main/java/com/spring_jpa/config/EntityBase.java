package com.spring_jpa.config;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Component
public class EntityBase {

    protected static Integer maxResult = 10;
    @Autowired
    public EntityManager entityManager;
    public static SessionFactory sessionFactory;

    public <T> CriteriaManager<T> createCriteria(Class<T> clazz) {
        return new CriteriaManager<T>(clazz, this);
    }

    public CriteriaQuery getCriteriaQuery(Class clazz) {
        return entityManager.getCriteriaBuilder().createQuery(clazz);
    }

    public <T> T get(Class<T> clazz, Serializable id) {
        if(id == null || "".equals(id)) {
            return null;
        }
        if(id instanceof String) {
            id = Integer.valueOf(id.toString());
        }
        return entityManager.find(clazz, id);
    }

    public Long count(Class entity) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = getCriteriaQuery(Long.class);
        cq.select(cb.count(cq.from(entity)));
        return entityManager.createQuery(cq).getSingleResult();
    }

    @Transactional
    public <T> T save(T entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Transactional
    public <T> List<T> save(List<T> entities) {
        entities.forEach(entity-> {
            save(entity);
        });
        return entities;
    }

    @Transactional
    public <T> T save(Class<T> clazz, Map<String, ?> properties) {
        T entity = get(clazz, (Serializable) properties.get("id"));
        if(entity == null) {
            try {
                entity = clazz.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        T finalEntity = entity;
        properties.forEach((k, v)-> {
            try {
                clazz.getMethod("set" + StringUtils.capitalize(k), v.getClass()).invoke(finalEntity, v);
            } catch (NoSuchMethodException e) {
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return this.save(entity);
    }

    @Transactional
    public void delete(Object entity) {
        entityManager.remove(entity);
    }

    @Transactional
    public void delete(List entities) {
        entities.forEach(entity -> {
            delete(entity);
        });
    }

}

