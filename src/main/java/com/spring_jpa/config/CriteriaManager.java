package com.spring_jpa.config;

import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.function.BiConsumer;

public class CriteriaManager<T> {
    final Class<T> clazz;
    public EntityManager entityManager;
    public EntityBase entityBase;
    public CriteriaBuilder criteriaBuilder;
    public CriteriaQuery<T> criteriaQuery;
    public Root<T> root;

    private Collection<Predicate> predicates = new ArrayList<>();

    public CriteriaManager(Class<T> clazz, EntityBase entityBase) {
        this.clazz = clazz;
        this.entityBase = entityBase;
        this.entityManager = entityBase.entityManager;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
        this.criteriaQuery = criteriaBuilder.createQuery(this.clazz);
        this.root = criteriaQuery.from(this.clazz);
    }

    public CriteriaManager<T> where(Predicate... predicate) {
        predicates.addAll(Arrays.asList(predicate));
        return this;
    }

    public CriteriaManager with(BiConsumer<CriteriaBuilder, Root<T>> predicate) {
        Objects.requireNonNull(predicate);
        predicate.accept(this.criteriaBuilder, this.root);
        return this;
    }

    public Long count(Map params) {
        buildCriteriaQuery((Class<T>) Long.class, params);
        criteriaQuery.select((Selection<? extends T>) criteriaBuilder.count(root));
        return (Long) entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    public Long count() {
        return count(new HashMap());
    }

    private Path getPath(String attr) {
        Path path = null;
        try {
            path = root.get(attr);
        } catch (Exception e) {
            return null;
        }
        return path;
    }

    private CriteriaQuery<T> buildCriteriaQuery(Map params) {
        return buildCriteriaQuery(clazz, params);
    }

    private CriteriaQuery<T> buildCriteriaQuery(Class<T> clazz, Map params) {
        String orderBy = params.get("orderBy") != null ? (String) params.get("orderBy") : "asc";

        if(!StringUtils.isEmpty(params.get("searchText"))) {
            String searchText = (String) params.get("searchText");
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.like(root.get("id"), "%" + searchText + "%"));
            if(getPath("name") != null) {
                predicates.add(criteriaBuilder.like(root.get("name"), "%" + searchText + "%"));
            }
            if(getPath("description") != null) {
                predicates.add(criteriaBuilder.like(root.get("description"), "%" + searchText + "%"));
            }
            criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[0])));
        }

        if(predicates.size() > 0) {
            criteriaQuery.where(predicates.toArray(new Predicate[0]));
        }

        if(orderBy == "asc") {
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get("id")));
        } else {
            criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));
        }

        return criteriaQuery;
    }

    public List<T> list() {
        return list(new HashMap<>());
    }

    public List<T> list(Map params) {
        Integer offset = params.get("page") != null ? Integer.parseInt((String) params.get("page")) : 0;
        Integer max = params.get("max") != null ? Integer.parseInt((String) params.get("max")) : entityBase.maxResult;
        Integer start = max * offset;

        buildCriteriaQuery(params);
        TypedQuery<T> typedQuery = entityManager.createQuery(criteriaQuery);

        typedQuery.setFirstResult(start);
        typedQuery.setMaxResults(max);

        List<T> items = typedQuery.getResultList();
        if (items.size() == 0 && start != 0) {
            typedQuery.setFirstResult(start - max);
            items = typedQuery.getResultList();
        }

        return items;
    }

}
