package com.spring_jpa.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Product extends ModelBase {
    @NotNull
    @NotBlank
    @Column(unique = true)
    private String name;

    private Double price = 0.0;

    public static void _init() {
        if(entityBase.count(Product.class) == 0) {
            List<Product> products = new ArrayList<>();
            for(int i = 1; i<= 15; i++) {
                Product product = new Product();
                product.setName("ProductForm " + i);
                product.setPrice(72.75);
                products.add(product);
            }
            entityBase.save(products);
        }
    }
}
