package com.spring_jpa.model;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class Tutorial extends ModelBase {
    private String title;
    private String description;
    private boolean published;
}
