package com.spring_jpa.model;

import com.spring_jpa.config.EntityBase;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
abstract public class ModelBase<T> implements Serializable {

    public static EntityBase entityBase;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, nullable = false)
    private Integer id;

    @NotNull
    @Column(updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    @PrePersist
    private void onCreate() {
        this.created = new Date();
    }

    @PreUpdate
    private void onUpdate() {
        this.updated = new Date();
    }

}
