package com.spring_jpa.util;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

public class HmacUtil {

    private static final String HMAC_SHA_256 = "HmacSHA256";

    private static final String HEADER_HAMC = "Hmac";

    // Expected format for the unencrypted data string.
    private static final String DATA_FORMAT = "id=%s&host=%s&url=%s&method=%s";

    private static final String HEADER_DATE = "Date";

    private static final String HEADER_CONTENT_SHA_256 = "Content-SHA-256";

    // Charset to use when encrypting a string.
    private static final String UTF_8 = "UTF-8";

    // A cryptographically secure random number generator.
    private static final SecureRandom secureRandom = new SecureRandom();

    private HmacUtil() {
    }

    public static Map<String, String> buildHmacHeader(String userName, String apiKey, String appId, String uri, String content, String method,
                                                      String timeStamp, String nonce) throws NoSuchAlgorithmException, InvalidKeyException {
        Map<String, String> headers = new HashMap<>();
        String contentSHA256 = calculateSHA256(content);
        final StringBuilder builder = new StringBuilder();

        builder.append(appId).append("\n");
        builder.append(uri).append("\n");
        builder.append(contentSHA256).append("\n");
        builder.append(method).append("\n");
        builder.append(timeStamp).append("\n");
        builder.append(nonce);
        String toSignData = builder.toString();

        String hmac = HmacUtil.calculateHMAC(toSignData, apiKey);

        headers.put(HEADER_HAMC, String.format("%s:%s:%s", userName, hmac, nonce));
        headers.put(HEADER_DATE, timeStamp);
        headers.put(HEADER_CONTENT_SHA_256, contentSHA256);

        return headers;
    }

    public static String createHmacHeader(String appId, String uri, String content, String method,
                                          String timeStamp, String nonce) throws NoSuchAlgorithmException,
            InvalidKeyException {
        final StringBuilder builder = new StringBuilder();

        builder.append(appId).append("\n");
        builder.append(uri).append("\n");
        builder.append(calculateSHA256(content)).append("\n");
        builder.append(method).append("\n");
        builder.append(timeStamp).append("\n");
        builder.append(nonce);// is a random or semi-random number that is generated for a specific use, "number used once" or "number once"

        return builder.toString();
    }

    public static String calculateSHA256(String content) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(content.getBytes(StandardCharsets.UTF_8));
        return new String(Base64.encodeBase64(digest.digest()));
    }

    // Encrypt a byte array using the provided key.
    public static byte[] hmacSha256(final byte[] data, final byte[] key)
            throws NoSuchAlgorithmException, InvalidKeyException {
        final Mac mac = Mac.getInstance(HMAC_SHA_256);
        mac.init(new SecretKeySpec(key, HMAC_SHA_256));
        return mac.doFinal(data);
    }

    // Generate a random byte array for cryptographic use.
    public static byte[] generateRandomBytes(final int size) {
        final byte[] key = new byte[size];
        secureRandom.nextBytes(key);
        return key;
    }

    public static String calculateHMAC(String data, String secret) throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKeySpec signingKey = new SecretKeySpec(secret.getBytes(), HMAC_SHA_256);
        Mac mac = Mac.getInstance(HMAC_SHA_256);
        mac.init(signingKey);
        byte[] rawHmac = mac.doFinal(data.getBytes());
        return new String(Base64.encodeBase64(rawHmac));
    }

    class HMAC_FORMAT {
        static String API_KEY;
        static String HMAC;
        static String TIME_STAMP;
    }
}
