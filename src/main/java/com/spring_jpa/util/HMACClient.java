package com.spring_jpa.util;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;


public class HMACClient {

    private final static String DATE_FORMAT = "EEE, d MMM yyyy HH:mm:ss z";
    private final static String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    private final static String SECRET_API_KEY = "secretsecret";
    private final static String APP_ID = "rds-remote-recovery";
    private final static String USERNAME = "jos";

    private static final Logger LOG = LoggerFactory.getLogger(HMACClient.class);

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        HMACClient client = new HMACClient();
        client.makeHTTPCallUsingHMAC(USERNAME);
    }

    public void makeHTTPCallUsingHMAC(String userName) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        String content = "{\"comment\" : {\"message\":\"blaat\" , \"from\":\"blaat\" , \"commentFor\":123}}";
        String contentType = "application/vnd.geo.comment+json";
        String timeStamp = new SimpleDateFormat(DATE_FORMAT).format(new Date());
        String nonce = DatatypeConverter.printHexBinary(HmacUtil.generateRandomBytes(16))
                .toLowerCase(Locale.US);
        HttpPost post = new HttpPost("http://localhost:9000/resources/rest/geo/comment");
        StringEntity data = new StringEntity(content, contentType);
        post.setEntity(data);

        String method = post.getMethod();
        Map<String, String> headers = HmacUtil.buildHmacHeader(userName, SECRET_API_KEY, APP_ID, post.getURI().getPath(),
                content, method, timeStamp, nonce);

        headers.forEach(post::addHeader);

        CloseableHttpClient client = HttpClients.createDefault();
        CloseableHttpResponse response = client.execute(post);

        System.out.println("client response:" + response.getStatusLine().getStatusCode());
    }
}