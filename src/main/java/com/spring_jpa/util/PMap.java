package com.spring_jpa.util;

import java.util.HashMap;
import java.util.Map;

public class PMap<K, V> extends HashMap {

    public PMap(Map<? extends K, ? extends V> m) {
        putAll(m);
    }

    public V get(Object key) {
        Object v = getOrDefault(key, null);
        if (v instanceof String && "".equals(v)) {
            return null;
        }
        return (V) v;
    }
}
