package com.spring_jpa.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
    public static Date getNow() {
        return Calendar.getInstance().getTime();
    }

    public static String getNow(String formate) {
        DateFormat dateFormat = new SimpleDateFormat(formate);
        return dateFormat.format(getNow());
    }

    public static Date currentDateTime() {
        return currentDateTime("Asia/Dhaka");
    }

    public static Date currentDateTime(String timeZone) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        return calendar.getTime();
    }
}
