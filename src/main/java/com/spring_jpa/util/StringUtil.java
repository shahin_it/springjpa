package com.spring_jpa.util;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class StringUtil {
    public static String toUnderScoreCase(String source) {
        if(source == null) {
            return null;
        }
        String regex = "([a-z])([A-Z]+)", replacement = "$1_$2";
        return source.replaceAll(regex, replacement).toLowerCase();
    }

    public static boolean isNullOrEmpty(String string) {
        if(string == null) {
            return true;
        }
        if(string.equals("")) {
            return true;
        }
        return false;
    }

    public static boolean isNullOrEmpty(Object string) {
        if(string == null) {
            return true;
        }
        return isNullOrEmpty(string.toString());
    }

    public static <T> List toList(T exp) {
        return Arrays.asList(exp);
    }

    public static String camelToTitleCase(String src) {
        return StringUtils.capitalize(src.replaceAll(String.format("%s|%s|%s",
                "(?<=[A-Z])(?=[A-Z][a-z])",
                "(?<=[^A-Z])(?=[A-Z])",
                "(?<=[A-Za-z])(?=[^A-Za-z])"), " "));
    }

    public static String toJson(Object object) {
        if (object != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                return objectMapper.writeValueAsString(object);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static Object jsonToObject(String aJson) {
        if (StringUtils.isNotBlank(aJson)) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                return objectMapper.readValue(aJson, Object.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static <T> T jsonToObject(String aJson, Class<T> aClass) {
        if (StringUtils.isNotBlank(aJson)) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                return (T) objectMapper.readValue(aJson, aClass);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static <T> T jsonToObject(String aJson, JavaType javaType) {
        if (StringUtils.isNotBlank(aJson)) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                return (T) objectMapper.readValue(aJson, javaType);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
