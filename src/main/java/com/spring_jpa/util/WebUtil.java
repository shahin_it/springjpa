package com.spring_jpa.util;

import java.util.HashMap;
import java.util.Map;

public class WebUtil {
    public static Map respMap(String message) {
        Map resp = new HashMap();
        resp.put("status", "success");
        resp.put("message", message);
        return resp;
    }

    public static String baseUrl() {
        String baseUrl = "";
        if (!StringUtil.isNullOrEmpty(baseUrl)) {
            baseUrl += "/";
        }
        return baseUrl;
    }
}
